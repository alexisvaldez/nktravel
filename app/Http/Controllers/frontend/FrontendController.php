<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class FrontendController extends Controller
{
    public function index()
    {
    	return view('frontend.index');
    }

    public function nosotros()
    {
    	return view('frontend.nosotros');
    }

    public function promociones()
    {
    	return view('frontend.promociones');
    }

    public function paquetes()
    {
    	return view('frontend.paquetes');
    }

    public function fulldays()
    {
    	return view('frontend.fulldays');
    }

    public function contacto()
    {
        return view('frontend.contacto');
    }
}
