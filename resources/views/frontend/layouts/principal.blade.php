<!doctype html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>NK Travel</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="manifest" href="site.webmanifest">
        <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
        <!-- Place favicon.ico in the root directory -->
        <!-- CSS here -->
        <link rel="stylesheet" href="{{asset('frontend/css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{asset('frontend/css/nice-select.css')}}">
        <link rel="stylesheet" href="{{asset('frontend/css/animate.css')}}">
        <link rel="stylesheet" href="{{asset('frontend/css/owl.carousel.min.css')}}">
        <link rel="stylesheet" href="{{asset('frontend/css/magnific-popup.css')}}">
        <link rel="stylesheet" href="{{asset('frontend/css/fontawesome-all.min.css')}}">
        <link rel="stylesheet" href="{{asset('frontend/css/meanmenu.css')}}">
        <link rel="stylesheet" href="{{asset('frontend/css/slick.css')}}">
        <link rel="stylesheet" href="{{asset('frontend/css/icofont.css')}}">
        <link rel="stylesheet" href="{{asset('frontend/css/jquery-ui.css')}}">
        <link rel="stylesheet" href="{{asset('frontend/css/default.css')}}">
        <link rel="stylesheet" href="{{asset('frontend/css/style.css')}}">
        <link rel="stylesheet" href="{{asset('frontend/css/responsive.css')}}">
        @yield('styles')

    </head>
    <body>
         <!--[if lte IE 9]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
        <![endif]-->
        <!-- Start Header Area -->
        <header class="header-area">
            <div class="container">
                <div class="header-top separator d-none d-md-block">
                    <div class="row">
                        <div class="col-lg-6 col-md-8">
                            <div class="call-to-action">
                                <span>Telefonos:</span>
                                <i class="fas fa-phone"></i>
                                +01 4431321 / 987 406 863 / 920 445 252
                            </div>
                        </div>
                        <div class="col-lg-5 col-md-2">
                            <div class="header-social-icon f-right">
                                <ul>
                                    <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                    <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fab fa-google-plus-g"></i></a></li>
                                    <li><a href="#"><i class="fab fa-pinterest-p"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-1 col-md-2">
                            <div class="user-account text-right">
                               
                            </div>
                        </div>
                    </div>
                </div>
                <div class="main-menu">
                    <div class="row">
                        <div class="col-lg-3 d-flex align-items-center">
                            <div class="logo">
                                <a href="index.html"><img src="{{asset('frontend/img/logo/logo.png')}}" alt="logo" width="160"></a>
                            </div>
                        </div>
                        <div class="col-lg-9">
                            <div class="basic-menu f-right">
                                <nav id="mobile-menu">
                                    <ul>
                                        <li>
                                            <a href="{{url('/')}}">INICIO</a>
                                        </li>
                                        <li>
                                            <a href="{{url('/nosotros')}}">NOSOTROS</a>
                                        </li>
                                        <li>
                                            <a href="{{url('/promociones')}}">PROMOCIONES</a>
                                        </li>
                                        <li>
                                            <a href="{{url('/paquetes')}}">PAQUETES <i class="icofont icofont-rounded-down"></i></a>
                                            <ul>
                                                <li><a href="{{url('/paquetes/nacionales')}}"> Nacionales</a></li>
                                                <li><a href="{{url('/paquetes/internacionales')}}">Internacionales</a></li>
                                            </ul>
                                        </li>
                                        <li>
                                            <a href="{{url('/fulldays')}}">FULL DAYS </a>
                                           
                                        </li>
                                        <li>
                                            <a href="{{url('/contacto')}}">ESCRIBENOS</a>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                            <div class="mobile-menu"></div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- End Header Area -->

        @yield('contenido')

        <!-- ========= Start Footer Area========= -->
        <!-- footer start -->
        <footer class="footer-area">
            <div class="footer-top-area pt-110 pb-50">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 col-md-5 ">
                            <div class="footer-widget mb-30">
                                <h4 class="white">CONTACTAR</h4>
                                <p class="white">Nuestras redes sociales se encuentra a tu disposicon para cualquier duda</p>
                                <ul class="footer-social-icon">
                                    <li><a href="www.facebook.com/agencianktravel/"><i class="fab fa-facebook-f"></i></a></li>
                                    <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fab fa-google-plus-g"></i></a></li>
                                    <li><a href="#"><i class="fab fa-pinterest-p"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-2">
                            <div class="footer-widget mb-30">
                                <h4 class="white">Servicios</h4>
                                <ul class="footer-link">
                                    <li>
                                        <a href="#">Nacionales</a>
                                    </li>
                                    <li>
                                        <a href="#">Internacionales</a>
                                    </li>
                                    <li>
                                        <a href="#">Full Days</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-2">
                            <div class="footer-widget mb-30">
                                <h4 class="white">Empresa</h4>
                                <ul class="footer-link">
                                    <li>
                                        <a href="#">Nosotros</a>
                                    </li>
                                    <li>
                                        <a href="#">Historia</a>
                                    </li>
                                    <li>
                                        <a href="#">Contactar</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3">
                            <div class="footer-widget mb-30">
                                <h4 class="white">Promociones</h4>
                                <ul class="footer-link">
                                    <li>
                                        <a href="#">Noviembre</a>
                                    </li>
                                    <li>
                                        <a href="#">Diciembre</a>
                                    </li>
                                    <li>
                                        <a href="#">Verano</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="copyright-area pb-50">
                <div class="container">
                    <p class="copyright text-center">TRAVELEXPRESS SERVICE U.S. DEPARTMENT OF INTERIOR</p>
                </div>
            </div>
        </footer>
        <!-- footer end -->

        <!-- JS here -->
        <script src="{{asset('frontend/js/vendor/modernizr-3.5.0.min.js')}}"></script>
        <script src="{{asset('frontend/js/vendor/jquery-1.12.4.min.js')}}"></script>
        <script src="{{asset('frontend/js/popper.min.js')}}"></script>
        <script src="{{asset('frontend/js/bootstrap.min.js')}}"></script>
        <script src="{{asset('frontend/js/owl.carousel.min.js')}}"></script>
        <script src="{{asset('frontend/js/isotope.pkgd.min.js')}}"></script>
        <script src="{{asset('frontend/js/jquery.nice-select.min.js')}}"></script>
        <script src="{{asset('frontend/js/slick.min.js')}}"></script>
        <script src="{{asset('frontend/js/jquery.meanmenu.min.js')}}"></script>
        <script src="{{asset('frontend/js/ajax-form.js')}}"></script>
        <script src="{{asset('frontend/js/jquery-ui.js')}}"></script>
        <script src="{{asset('frontend/js/waypoint.js')}}"></script>
        <script src="{{asset('frontend/js/jquery.counterup.min.js')}}"></script>
        <script src="{{asset('frontend/js/imagesloaded.pkgd.min.js')}}"></script>
        <script src="{{asset('frontend/js/jquery.magnific-popup.min.js')}}"></script>
        <script src="{{asset('frontend/js/plugins.js')}}"></script>
        <script src="{{asset('frontend/js/main.js')}}"></script>
        @yield('scripts')
    </body>
</html>
