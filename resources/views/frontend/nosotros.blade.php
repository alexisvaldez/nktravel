@extends('frontend.layouts.principal')

@section('contenido')
<section class="breadcrumb-area" data-overlay="5" style="background-image: url({{asset('frontend/img/bg/about-breadcrumb.jpg')}});">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="breadcrumb-wrapper text-center">
                            <h3>Sobre Nosotros</h3>
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item"><a href="index.html">Inicio</a></li>
                                <li class="breadcrumb-item active">nosotros</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Banner Area -->
        <!-- Start About History -->
        <section class="about-history-area pt-110 pb-120">
            <div class="container">
                <div class="section-title text-center pb-80">
                    <h2>Nuestra 
                        <span>Historia</span>
                    </h2>
                    <p>NK Travel se inicia por la necesidad de llevar de excursión a un grupo de personas adultos mayores que salían con frecuencia fuera de Lima, nos dieron la oportunidad de poder llevarlos y poder ayudarlos a que tengan una nueva experiencia de viaje, es así como empezamos con esta aventura viajera asesoramos y seguimos cumpliendo sueños a muchos pasajeros..</p>
                </div>
                <div class="row justify-content-center">
                    <div class="col-lg-10">
                        <div class="video-box">
                            <div class="about-history-content">
                                <img src="{{asset('frontend/img/bg/video-img-2.jpg')}}" alt="about-img">
                                <a href="https://www.youtube.com/watch?v=3xJzYpRVQVA" class="video-play home-2">
                                    <i class="fas fa-play"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End About History -->
        <!-- Start Facts Up Area -->
        <section class="facts-area pt-300 pb-90" data-overlay="3">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-10">
                        <div class="row">
                            <div class="col-lg-3 col-md-3 col-sm-6">
                                <div class="single-facts-item text-center mb-30">
                                    <span class="counter">4000</span>+
                                    <h5>Kilometros de viajes</h5>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-6">
                                <div class="single-facts-item text-center mb-30">
                                    <span class="counter">79</span>K
                                    <h5>Viajes realizados</h5>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-6">
                                <div class="single-facts-item text-center mb-30">
                                    <span class="counter">70</span>K
                                    <h5>Clientes Felices</h5>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-6">
                                <div class="single-facts-item text-center mb-30">
                                    <span class="counter">2015</span>
                                    <h5>Iniciamos</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Facts Up Area -->
        <section class="testimonial-area pt-110 ">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="testimonial-active owl-carousel">
                            <div class="testimonial-single-item">
                                
                                <h4>Somos una agencia de viajes peruana dedicada a brindar experiencias increíbles en el sector del turismo. Ofrecemos un servicio personalizado y de calidad. Atendemos: Venta de Boletos aéreos, venta de hoteles, paquetes turísticos, seguros, excursiones corporativas, excursiones full day y transporte.</h4>
                                
                            </div> <!-- end single testimonial item -->
                           
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="faq-area">

                            <div class="faq-content">
                                <div id="accordion">
                                    <div class="card">
                                        <div class="card-header" id="item1">
                                            <h5 class="mb-0">
                                                <button class="btn faq" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                VISION
                                                </button>
                                            </h5>
                                        </div>
                                        <div id="collapseOne" class="collapse show" aria-labelledby="item1" data-parent="#accordion">
                                            <div class="card-body">
                                                Ser reconocida como una Agencia que brinda una asesoría impecable y completa en experiencias de viaje para nuestros clientes, a su vez confíen en nosotros de forma constante y transmitir la seguridad total para su viaje.
                                                Crecer en el mercado del Turismo como Agencia de Viajes innovadora en servicios y sobre todo en calidad de servicio.
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-header" id="item2">
                                            <h5 class="mb-0">
                                                <button class="btn faq collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                MISION
                                                </button>
                                             </h5>
                                        </div>
                                        <div id="collapseTwo" class="collapse" aria-labelledby="item2" data-parent="#accordion">
                                            <div class="card-body">
                                               Ofrecer un excelente servicio, dando un trato personalizado, ubicando su requerimiento no sólo como un viaje sino como una experiencia de vida, con la ayuda de la innovación, tecnología y sobre todo calidad humana de nuestra empresa para cumplir los requerimientos solicitados.
                                               Dando algo más que una simple asesoría, siempre cumpliendo las expectativas de nuestros clientes.

                                            </div>
                                        </div>
                                    </div>
                                  
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- feature-area start -->
        <section class="feature-area">
            <div class="container">
                <div class="separator pt-115 pb-110">
                    <div class="row">
                        <div class="col-lg-4 col-md-4">
                            <a href="">
                                <div class="single-feature-item d-lg-flex align-items-center">
                                    <div class="feature-img">
                                        <img src="{{asset('frontend/img/icon/tower.png')}}" alt="feature-img">
                                    </div>
                                    <div class="feature-content">
                                        <h4>PAQUETES</h4>
                                        <p>Nacional e Internacional.</p>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-4 col-md-4">
                            <a href="">
                                <div class="single-feature-item d-lg-flex align-items-center">
                                    <div class="feature-img">
                                        <img src="{{asset('frontend/img/icon/rate.png')}}" alt="feature-img">
                                    </div>
                                    <div class="feature-content">
                                        <h4>FULL DAYS</h4>
                                        <p>Grupos.</p>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-4 col-md-4">
                            <a href="">
                                <div class="single-feature-item d-lg-flex align-items-center">
                                    <div class="feature-img">
                                        <img src="{{asset('frontend/img/icon/notebook.png')}}" alt="feature-img">
                                    </div>
                                    <div class="feature-content">
                                        <h4>PROMOCIONES</h4>
                                        <p>Descuentos hasta 50%.</p>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Feature Area -->
        <!-- Start Promote Area -->
        <section class="promote-area home-v pt-110 pb-120" data-overlay="8">
            <div class="container">
                <div class="promote-content text-center">
                    <h2 class="white">OBTENGA <span>10-20% </span>de descuento </h2>
                    <h4 class="white">CUALQUIER PAQUETE DEL TOUR</h4>
                    <a class="btn-1" href="#">mas informacion</a>
                </div>
            </div>
        </section>
@endsection