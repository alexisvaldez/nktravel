@extends('frontend.layouts.principal')

@section('contenido')
<section class="breadcrumb-area about-page" data-overlay="5" style="background-image: url({{asset('frontend/img/bg/about-breadcrumb.jpg')}});">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="breadcrumb-wrapper text-center">
                            <h3>Contacto</h3>
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{url('/')}}">Inicio</a></li>
                                <li class="breadcrumb-item active">Contacto</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Start Contact area -->
        <div class="contact-area mt-100 mb-100">
            <div class="container">
                <div class="contact-info">
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-6">
                            <div class="contact-single-info mb-30">
                                394 University Ave.<br>
                                Paolo Alto, CA 957236<br>
                                +56669992211
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6">
                            <div class="contact-single-info mb-30">
                                Briger Jarlsgatan 11<br>
                                225 25 Stockholm<br>
                                +98669992211
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6">
                            <div class="contact-single-info mb-30">
                                3Byraggetgata 9<br>
                                0365 Oslo<br>
                                +12375992211
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6">
                            <div class="contact-single-info mb-30">
                                394 University Ave.<br>
                                Paolo Alto, CA 957236<br>
                                +56669992211
                            </div>
                        </div>
                    </div>
                </div>
                <div class="contact-field">
                    <form id="ajax-contact" action="mail.php" method="post">
                        <div class="row">
                            <div class="col-lg-6 col-md-6">
                                <div class="single-contact-field">
                                    <input type="text"  name="name" class="text-field" id="name" placeholder="First Name" required>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="single-contact-field">
                                    <input type="text"  name="last_name" class="text-field" id="l-name" placeholder="Last Name" required>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="single-contact-field">
                                    <input type="email"  name="email" class="text-field" id="email" placeholder="Your Email" required>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="single-contact-field">
                                    <input type="text"  name="phone" class="text-field" id="phone" placeholder="Phone Number" required>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12">
                                <div class="single-contact-field">
                                    <input type="text"  name="subject" class="text-field" id="subject" placeholder="Subject" required>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12">
                                <div class="single-contact-field">
                                    <textarea name="message" id="msg" placeholder="Your Message"></textarea>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 text-center">
                                <button type="submit" class="submit-btn">Enviar</button>
                            </div>
                        </div>
                        <div id="form-messages"></div>
                    </form>
                </div>
            </div>
        </div>
@endsection
@section('scripts')

@endsection