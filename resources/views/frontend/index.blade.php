@extends('frontend.layouts.principal')

@section('contenido')
<!-- ========= Start Slider Area========= -->
<section class="slider-area">
    <div class="slider-active owl-carousel">
        <div class="single-slider" style="background: url({{asset('frontend/img/slider/slider.jpg')}});">
            <div class="container">
                <div class="slider-content text-center">
                    <h1>PARACAS FULL DAYS</h1>
                    <h4>Tubulares, paseo por y mas</h4>
                    <a class="btn-1" href="#">Ir a tours</a>
                </div>
            </div>
        </div>
        <div class="single-slider" style="background: url({{asset('frontend/img/slider/slider.jpg')}});">
            <div class="container">
                <div class="slider-content text-center">
                    
                </div>
            </div>
        </div>
        <div class="single-slider" style="background: url({{asset('frontend/img/slider/slider.jpg')}});">
            <div class="container">
                <div class="slider-content text-center">
                    
                </div>
            </div>
        </div>
    </div>
</section> <!-- End Slider Area -->
<!-- ========= Start Booking Area========= -->
<div class="booking-area">
    <div class="container">
        <div class="booking-option">
            
            <div class="form-action form-action-one">
                <ul class="select-bar">
                    <li class="location">
                        <select name="location" class="location-here">
                            <option value="Dhaka">Location </option>
                            <option value="Rajshahi">Rajshahi</option>
                            <option value="Chitagong">Chitagong</option>
                            <option value="SKhulna">SKhulna</option>
                        </select>
                    </li>
                    <li class="location">
                        <form class="icon" action="#">
                            <input class="datepicker" type="text" placeholder="Check in">
                        </form>
                    </li>
                    <li class="location">
                        <form class="icon" action="#">
                            <input class="datepicker" type="text" placeholder="Checkout">
                        </form>
                    </li>
                    <li class="location">
                        <select name="location" class="location-here">
                            <option value="Dhaka">Room</option>
                            <option value="Rajshahi">Single</option>
                            <option value="Chitagong">Double</option>
                            <option value="SKhulna">Double</option>
                        </select>
                    </li>
                    <li class="location">
                        <select name="location" class="location-here member">
                            <option value="Dhaka">Member</option>
                            <option value="Rajshahi">1 Person</option>
                            <option value="Chitagong">2 Person</option>
                            <option value="SKhulna">3 Person</option>
                        </select>
                    </li>
                </ul>
                <div class="filter-part">
                    <div class="range-wrapper">
                        <div class="range-input">
                            <input type="text" id="amount" readonly style="border:0; color:#f6931f; font-weight:bold;">
                        </div>
                        <label for="amount">Budget Now :</label>
                        <div id="slider-range"></div>
                    </div>
                    <div class="filter-button">
                        <button type="submit">SEARCH NOW</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ========= Start Feature Area========= -->
<!-- feature-area start -->
<section class="feature-area">
    <div class="container">
        <div class="separator pt-115 pb-110">
            <div class="row">
                <div class="col-lg-4 col-md-4">
                    <a href="">
                        <div class="single-feature-item d-lg-flex align-items-center">
                            <div class="feature-img">
                                <img src="{{asset('frontend/img/icon/tower.png')}}" alt="feature-img">
                            </div>
                            <div class="feature-content">
                                <h4>PAQUETES</h4>
                                <p>Nacional e Internacional.</p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-md-4">
                    <a href="">
                        <div class="single-feature-item d-lg-flex align-items-center">
                            <div class="feature-img">
                                <img src="{{asset('frontend/img/icon/rate.png')}}" alt="feature-img">
                            </div>
                            <div class="feature-content">
                                <h4>FULL DAYS</h4>
                                <p>Grupos.</p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-md-4">
                    <a href="">
                        <div class="single-feature-item d-lg-flex align-items-center">
                            <div class="feature-img">
                                <img src="{{asset('frontend/img/icon/notebook.png')}}" alt="feature-img">
                            </div>
                            <div class="feature-content">
                                <h4>PROMOCIONES</h4>
                                <p>Descuentos hasta 50%.</p>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Feature Area -->
<section class="package-area home-package pt-110 pb-90" data-overlay="3">
    <div class="container">
        <div class="section-title text-center pb-45">
            <h2 class="white">Paquetes Destacados</h2>
            <h4 class="white">Nacional e Internacioles</h4>
        </div>
        <div class="row">
            <div class="col-lg-4">
                <div class="single-package mb-30">
                    <div class="package-img">
                        <img src="{{asset('frontend/img/package/pack-1.jpg')}}" alt="package image">
                    
                    </div>
                    <h5 class="white"><span>SELVA CENTRAL</span> - TOURS</h5>
                    <p class="white">Una aventura diferente conoce la selva central y todo lo que ella tieen para ti.</p>
                    <a class="read-more" href="#">ver mas <i class="icofont icofont-arrow-right"></i></a>
                </div>
            </div> <!-- End Single Package -->
            <div class="col-lg-4">
                <div class="single-package mb-30">
                    <div class="package-img">
                        <img src="{{asset('frontend/img/package/pack-2.jpg')}}" alt="package image">
                        
                    </div>
                    <h5 class="white"><span>ICA</span> - 4 dias, 3 noches</h5>
                    <p class="white">Viaja a ica y desetresate de lima .</p>
                    <a class="read-more" href="#">ver mas <i class="icofont icofont-arrow-right"></i></a>
                </div>
            </div> <!-- End Single Package -->
            <div class="col-lg-4 ">
                <div class="single-package mb-30">
                    <div class="package-img">
                        <img src="{{asset('frontend/img/package/pack-3.jpg')}}" alt="package image">
                        
                    </div>
                    <h5 class="white"><span>MIAMI</span> - 3 dias, 2 noches</h5>
                    <p class="white">El viaje de tus suenos esta aun solo clic</p>
                    <a class="read-more" href="#">ver mas <i class="icofont icofont-arrow-right"></i></a>
                </div>
            </div> <!-- End Single Package -->
        </div>
        <div class="more-package text-center">
            <a class="btn-1 more-pack" href="#">ver mas paquetes</a>
        </div>
    </div>
</section>
<!-- ========= Start Popular Place Area========= -->
<section class="popular-place pt-110 pb-90">
    <div class="container">
        <div class="section-title text-center pb-60">
            <h2>Full Days</h2>
            <h4>No necesitas vacaciones para irte de viaje.</h4>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="pp-list-item">
                    <div class="single-pp">
                        <div class="single-pp-thumb">
                            <img src="{{asset('frontend/img/package/pp-1.jpg')}}" alt="popular image">
                        </div>
                        <div class="pp-wrapper-content">
                            <div class="popular-pp-info">
                                <a href="#">
                                    <h5>PARACAS – ICA</h5>
                                </a>
                                <p>Visitas todo paracas, 5 dias, 4 Noches</p>
                             
                            </div>
                           
                        </div>
                    </div>
                </div>
                <div class="pp-list-item">
                    <div class="single-pp">
                        <div class="single-pp-thumb">
                            <img src="{{asset('frontend/img/package/pp-2.jpg')}}" alt="popular image">
                        </div>
                        <div class="popular-pp-info">
                            <a href="#"><h5>Azpitia</h5></a>
                            <p>Un dia de aventura y relax</p>
                            
                        </div>
                       
                    </div>
                </div>
               
                
            </div>
            <div class="col-lg-6">
                <div class="pp-list-item-right">
                    <div class="single-pp">
                        <div class="single-pp-thumb">
                            <img src="{{asset('frontend/img/package/pp-5.jpg')}}" alt="popular image">
                        </div>
                        <div class="popular-pp-info">
                            <a href="#"><h5>A pie por Lima</h5></a>
                            <p>Por lima Peru, 1 dia, 1 noche Tour</p>
                            
                        </div>
                        
                    </div>
                </div>
                <div class="pp-list-item-right">
                    <div class="single-pp">
                        <div class="single-pp-thumb">
                            <img src="{{asset('frontend/img/package/pp-6.jpg')}}" alt="popular image">
                        </div>
                        <div class="popular-pp-info">
                            <a href="#"><h5>Marcapomacocha</h5></a>
                            <p> Provincia de Yauli</p>
                        </div>
                        
                    </div>
                </div>
               
                
            </div>
        </div>
    </div>
</section>
@endsection

        