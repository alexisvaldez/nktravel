@extends('frontend.layouts.principal')

@section('contenido')
 <section class="breadcrumb-area" data-overlay="5" style="background-image: url({{asset('frontend/img/bg/about-breadcrumb.jpg')}})">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="breadcrumb-wrapper text-center">
                            <h3>Promociones</h3>
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{url('/')}}">Inicio</a></li>
                                <li class="breadcrumb-item active">Promociones</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Banner Area -->
        <!-- Start Blog page -->
        <section class="blog-page-area light-bg pt-110 pb-120">
            <div class="container">
                <div class="row blog-grid">
                    <div class="col-lg-4 grid-item mb-30">
                        <div class="single-blog-item white-bg">
                            <div class="single-blog-info">
                                <span>TRAVELING</span>
                                <h4><a href="#">TRAVEL IS THE BEST PART OF LIFE TO CREATION.</a></h4>
                                <p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur sequi nesciunt.</p>
                                <div class="blog-meta">
                                    <div class="meta-icon">
                                        {{-- <a href="#"><i class="fas fa-home"></i></a>
                                        <a href="#"><i class="far fa-heart"></i></a> --}}
                                    </div>
                                    <div class="post-date">
                                        <a href="#"><i class="far fa-clock"></i>May 10, 2017</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end single post -->
                    <div class="col-lg-4 grid-item mb-30">
                        <div class="single-blog-item white-bg">
                            <div class="single-blog-thumb">
                                <img src="{{asset('frontend/img/blog/blog-s2.jpg')}}" alt="post-image">
                            </div>
                            <div class="single-blog-info">
                                <h4><a href="#">LONIAN ISLAND, GREECE</a></h4>
                                <p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur sequi nesciunt.</p>
                                <div class="blog-meta">
                                    <div class="meta-icon">
                                        {{-- <a href="#"><i class="fas fa-home"></i></a>
                                        <a href="#"><i class="far fa-heart"></i></a> --}}
                                    </div>
                                    <div class="post-date">
                                        <a href="#"><i class="far fa-clock"></i>May 10, 2017</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> <!-- end single post -->
                    <div class="col-lg-4 grid-item mb-30">
                        <div class="single-blog-item white-bg">
                            <div class="single-blog-info">
                                <span>TRAVELING</span>
                                <h4><a href="#">TRAVEL IS THE BEST PART OF LIFE TO CREATION.</a></h4>
                                <p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur sequi nesciunt.</p>
                                <div class="blog-meta">
                                    <div class="meta-icon">
                                        {{-- <a href="#"><i class="fas fa-home"></i></a>
                                        <a href="#"><i class="far fa-heart"></i></a> --}}
                                    </div>
                                    <div class="post-date">
                                        <a href="#"><i class="far fa-clock"></i>May 10, 2017</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> <!-- end single post -->
                    <div class="col-lg-4 grid-item mb-30">
                        <div class="single-blog-item white-bg">
                            <div class="single-blog-thumb">
                                <img src="{{asset('frontend/img/blog/blog-s.jpg')}}" alt="post-image">
                            </div>
                            <div class="single-blog-info">
                                <h4><a href="#">LONIAN ISLAND, GREECE</a></h4>
                                <div class="blog-meta">
                                    <div class="meta-icon">
                                        {{-- <a href="#"><i class="fas fa-home"></i></a>
                                        <a href="#"><i class="far fa-heart"></i></a> --}}
                                    </div>
                                    <div class="post-date">
                                        <a href="#"><i class="far fa-clock"></i>May 10, 2017</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> <!-- end single post -->
                    <div class="col-lg-4 grid-item mb-30">
                        <div class="single-blog-item white-bg">
                            <div class="single-blog-thumb">
                                <img src="{{asset('frontend/img/blog/blog-img1.jpg')}}" alt="post-image">
                            </div>
                            <div class="single-blog-info">
                                <h4><a href="#">LONIAN ISLAND, GREECE</a></h4>
                                <div class="blog-meta">
                                    <div class="meta-icon">
                                        {{-- <a href="#"><i class="fas fa-home"></i></a>
                                        <a href="#"><i class="far fa-heart"></i></a> --}}
                                    </div>
                                    <div class="post-date">
                                        <a href="#"><i class="far fa-clock"></i>May 10, 2017</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> <!-- end single post -->
                    <div class="col-lg-4 grid-item mb-30">
                        <div class="single-blog-item white-bg">
                            <div class="single-blog-thumb">
                                <img src="{{asset('frontend/img/blog/blog-l.jpg')}}" alt="post-image">
                            </div>
                            <div class="single-blog-info">
                                <h4><a href="#">LONIAN ISLAND, GREECE</a></h4>
                                <p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur sequi nesciunt.</p>
                                <div class="blog-meta">
                                    <div class="meta-icon">
                                        {{-- <a href="#"><i class="fas fa-home"></i></a>
                                        <a href="#"><i class="far fa-heart"></i></a> --}}
                                    </div>
                                    <div class="post-date">
                                        <a href="#"><i class="far fa-clock"></i>May 10, 2017</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end single post -->
                    <div class="col-lg-4 grid-item mb-30">
                        <div class="single-blog-item white-bg">
                            <div class="single-blog-info">
                                <span>TRAVELING</span>
                                <h4>
                                    <a href="#">TRAVEL IS THE BEST PART OF LIFE TO CREATION.</a>
                                </h4>
                                <p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur sequi nesciunt.</p>
                                <div class="blog-meta">
                                    <div class="meta-icon">
                                        <a href="#">
                                            <i class="fas fa-home"></i>
                                        </a>
                                        <a href="#">
                                            <i class="far fa-heart"></i>
                                        </a>
                                    </div>
                                    <div class="post-date">
                                        <a href="#">
                                            <i class="far fa-clock"></i>May 10, 2017</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 grid-item mb-30">
                        <div class="single-blog-item white-bg">
                            <div class="single-blog-info">
                                <span>TRAVELING</span>
                                <h4>
                                    <a href="#">TRAVEL IS THE BEST PART OF LIFE TO CREATION.</a>
                                </h4>
                                <p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur sequi nesciunt.</p>
                                <div class="blog-meta">
                                    <div class="meta-icon">
                                        <a href="#">
                                            <i class="fas fa-home"></i>
                                        </a>
                                        <a href="#">
                                            <i class="far fa-heart"></i>
                                        </a>
                                    </div>
                                    <div class="post-date">
                                        <a href="#">
                                            <i class="far fa-clock"></i>May 10, 2017</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="add-more-post text-center">
                            <a href="#"> Ver más</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
@endsection