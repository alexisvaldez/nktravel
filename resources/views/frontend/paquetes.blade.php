@extends('frontend.layouts.principal')

@section('contenido')
<section class="breadcrumb-area about-page" data-overlay="5" style="background-image: url({{asset('frontend/img/bg/about-breadcrumb.jpg')}});">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="breadcrumb-wrapper text-center">
                            <h3>Paquetes</h3>
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{url('/')}}">Inicio</a></li>
                                <li class="breadcrumb-item active">Paquetes</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Banner Area -->
       <section class="package-area pt-110 pb-118">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="package-top-bar mb-60 separator clearfix">
                            <div class="package-search f-left">
                                <form action="#">
                                    <input type="text" placeholder="Search Now">
                                    <button><i class="fas fa-search"></i></button>
                                </form>
                            </div>
                            <div class="package-search-result f-right">
                                <p>Showing 1- 5 of 20 results</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-md-6">
                        <div class="single-package mb-50">
                            <div class="package-img">
                                <img src="{{asset('frontend/img/package/pack-1.jpg')}}" alt="package image">
                                <div class="package-cost">$3000</div>
                            </div>
                            <h5><span>AMERICA</span> - 5 Days, 4 Nights</h5>
                            <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected.</p>
                            <a class="read-more" href="#">READ MORE <i class="icofont icofont-arrow-right"></i></a>
                        </div>
                    </div> <!-- End Single Package -->
                    <div class="col-lg-4 col-md-6">
                        <div class="single-package mb-50">
                            <div class="package-img">
                                <img src="{{asset('frontend/img/package/pack-2.jpg')}}" alt="package image">
                                <div class="package-cost">$3000</div>
                            </div>
                            <h5><span>THAILAND</span> - 4 Days, 3 Nights</h5>
                            <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected.</p>
                            <a class="read-more" href="#">READ MORE <i class="icofont icofont-arrow-right"></i></a>
                        </div>
                    </div> <!-- End Single Package -->
                    <div class="col-lg-4 col-md-6">
                        <div class="single-package mb-50">
                            <div class="package-img">
                                <img src="{{asset('frontend/img/package/pack-3.jpg')}}" alt="package image">
                                <div class="package-cost">$3000</div>
                            </div>
                            <h5><span>JAPAN</span> - 3 Days, 2 Nights</h5>
                            <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected.</p>
                            <a class="read-more" href="#">READ MORE <i class="icofont icofont-arrow-right"></i></a>
                        </div>
                    </div> <!-- End Single Package -->
                    <div class="col-lg-4 col-md-6">
                        <div class="single-package mb-50">
                            <div class="package-img">
                                <img src="{{asset('frontend/img/package/pack-1.jpg')}}" alt="package image">
                                <div class="package-cost">$3000</div>
                            </div>
                            <h5><span>AMERICA</span> - 5 Days, 4 Nights</h5>
                            <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected.</p>
                            <a class="read-more" href="#">READ MORE <i class="icofont icofont-arrow-right"></i></a>
                        </div>
                    </div> <!-- End Single Package -->
                    <div class="col-lg-4 col-md-6">
                        <div class="single-package mb-50">
                            <div class="package-img">
                                <img src="{{asset('frontend/img/package/pack-2.jpg')}}" alt="package image">
                                <div class="package-cost">$3000</div>
                            </div>
                            <h5><span>THAILAND</span> - 4 Days, 3 Nights</h5>
                            <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected.</p>
                            <a class="read-more" href="#">READ MORE <i class="icofont icofont-arrow-right"></i></a>
                        </div>
                    </div> <!-- End Single Package -->
                    <div class="col-lg-4 col-md-6">
                        <div class="single-package mb-50">
                            <div class="package-img">
                                <img src="{{asset('frontend/img/package/pack-3.jpg')}}" alt="package image">
                                <div class="package-cost">$3000</div>
                            </div>
                            <h5><span>JAPAN</span> - 3 Days, 2 Nights</h5>
                            <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected.</p>
                            <a class="read-more" href="#">READ MORE <i class="icofont icofont-arrow-right"></i></a>
                        </div>
                    </div> <!-- End Single Package -->
                    <div class="col-lg-4 col-md-6">
                        <div class="single-package mb-50">
                            <div class="package-img">
                                <img src="{{asset('frontend/img/package/pack-2.jpg')}}" alt="package image">
                                <div class="package-cost">$3000</div>
                            </div>
                            <h5><span>AMERICA</span> - 5 Days, 4 Nights</h5>
                            <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected.</p>
                            <a class="read-more" href="#">READ MORE <i class="icofont icofont-arrow-right"></i></a>
                        </div>
                    </div> <!-- End Single Package -->
                    <div class="col-lg-4 col-md-6">
                        <div class="single-package mb-50">
                            <div class="package-img">
                                <img src="{{asset('frontend/img/package/pack-1.jpg')}}" alt="package image">
                                <div class="package-cost">$3000</div>
                            </div>
                            <h5><span>THAILAND</span> - 4 Days, 3 Nights</h5>
                            <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected.</p>
                            <a class="read-more" href="#">READ MORE <i class="icofont icofont-arrow-right"></i></a>
                        </div>
                    </div> <!-- End Single Package -->
                    <div class="col-lg-4 col-md-6">
                        <div class="single-package mb-50">
                            <div class="package-img">
                                <img src="{{asset('frontend/img/package/pack-3.jpg')}}" alt="package image">
                                <div class="package-cost">$3000</div>
                            </div>
                            <h5><span>JAPAN</span> - 3 Days, 2 Nights</h5>
                            <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected.</p>
                            <a class="read-more" href="#">READ MORE <i class="icofont icofont-arrow-right"></i></a>
                        </div>
                    </div> <!-- End Single Package -->
                </div>
                <div class="row pt-50">
                    <div class="col-lg-12">
                        <div class="pagination-area">
                            <nav aria-label="Page navigation">
                                <ul class="pagination">
                                    <li class="page-item">
                                        <a class="page-link pagi-icon" href="#">
                                            <i class="fas fa-chevron-left" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                    <li class="page-item">
                                        <a class="page-link" href="#">1</a>
                                    </li>
                                    <li class="page-item">
                                        <a class="page-link" href="#">2</a>
                                    </li>
                                    <li class="page-item active">
                                        <a class="page-link" href="#">3</a>
                                    </li>
                                    <li class="page-item">
                                        <a class="page-link" href="#">4</a>
                                    </li>
                                    <li class="page-item">
                                        <a class="page-link" href="#">5</a>
                                    </li>
                                    <li class="page-item">
                                        <a class="page-link pagi-icon" href="#">
                                            <i class="fas fa-chevron-right" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Start Promote Area -->
        <section class="promote-area gray-bg pt-110 pb-120">
            <div class="container">
                <div class="promote-content text-center">
                    <h2>GET DISCOUNT <span>10-20% </span>OFF ANY TOUR PACKAGE</h2>
                    <h4>WHEN YOU PURCHASE ANY PACKAGE & GET NEXT TOUR</h4>
                    <a class="btn-1" href="#">START YOUR TRIP NOW</a>
                </div>
            </div>
        </section>
@endsection