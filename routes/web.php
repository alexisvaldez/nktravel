<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', 'frontend\FrontendController@index');
Route::get('/nosotros', 'frontend\FrontendController@nosotros');
Route::get('/promociones', 'frontend\FrontendController@promociones');
Route::get('/paquetes', 'frontend\FrontendController@paquetes');
Route::get('/fulldays', 'frontend\FrontendController@fulldays');
Route::get('/contacto', 'frontend\FrontendController@contacto');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
